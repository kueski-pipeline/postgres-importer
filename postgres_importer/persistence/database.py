import datetime
import logging
from typing import List, Dict, Any, Tuple

import psycopg


class DatabaseClient:

    def __init__(self, username: str, password: str, host: str, port: int, database: str, schema: str, table: str):

        self._database = database
        self._schema = schema
        self._table = table

        self._connection = psycopg.connect(user=username, password=password, host=host, port=port, dbname=database)
        self._connection.autocommit = False
        self._cursor = self._connection.cursor()

    def insert(self, batch: List[Dict[str, Any]]):
        for row in batch:
            self._run_insert_statement(row)
        self._connection.commit()
        logging.debug("Batch commited")

    def _run_insert_statement(self, row: Dict[str, Any]):
        filtered_row = {key: value for key, value in row.items() if value is not None}

        if len(filtered_row.keys()) <= 3:
            return None

        column_names = ", ".join(filtered_row.keys())
        placeholder = ", ".join(["%s"]*len(filtered_row.keys()))

        statement = f"INSERT INTO {self._schema}.{self._table} ({column_names}) VALUES ({placeholder})"

        logging.debug(f"preparing statement: {statement} for row {filtered_row}")
        values = tuple(filtered_row.values())
        self._cursor.execute(statement, values)
