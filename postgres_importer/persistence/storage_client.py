from datetime import datetime
import json
import os
import logging
from time import strftime
from typing import List, Tuple, Dict, Any, Union
from uuid import uuid4

import boto3
from smart_open import open

COLLECTED_DATETIME_FORMAT = "%Y-%m-%d %H:%M"


class StorageClient:

    def __init__(self, output_folder: str, collected_datetime_column: str, collected_datetime: datetime):

        self._output_folder = output_folder
        self._collected_datetime_column = collected_datetime_column
        self._collected_datetime = collected_datetime.strftime(COLLECTED_DATETIME_FORMAT)

    def insert(self, tables_batch: List[Tuple[str, Dict[str, Any]]]):

        for table, row in tables_batch:
            row_with_extra_fields = {**row, self._collected_datetime_column: self._collected_datetime}
            self._insert_batch(table, row_with_extra_fields)
        logging.debug("Batch commited")

    def _create_storage_client(self):
        endpoint = os.environ.get("S3_ENDPOINT")

        if endpoint:
            aws_access_key_id = os.environ.get("AWS_ACCESS_KEY_ID")
            aws_secret_access_key = os.environ.get("AWS_SECRET_ACCESS_KEY")

            session = boto3.Session()
            client = session.client('s3', endpoint_url=endpoint, use_ssl=False,
                                    aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
            transport_params = dict(client=client)
        else:
            transport_params = None

        return transport_params

    def _insert_batch(self, table, row: Dict[str, Any]):

        filtered_row = {key: _format_if_date(value) for key, value in row.items() if value is not None}
        if len(filtered_row.keys()) <= 3:
            return

        output_path = f"{self._output_folder}/{table}/{uuid4()}.json"
        transport_params = self._create_storage_client()

        with open(output_path, mode="w", transport_params=transport_params) as output_file:
            logging.debug(f"Inserting row into {output_path} file: {filtered_row}")
            json.dump(filtered_row, output_file)


def _format_if_date(value: Union[datetime, Any]) -> Union[str, int, float]:

    if isinstance(value, datetime):
        return value.strftime(COLLECTED_DATETIME_FORMAT)

    return value
