from typing import Generator, Any, Dict, List

import os
import json
import boto3

from smart_open import open, s3


def load_json(source_folder: str, batch_size: int) -> Generator[List[Dict[str, Any]], None, None]:

    for file_path in _list_files(source_folder):
        content_lines = _load_json_file(file_path)

        batch = []

        for content_line in content_lines:
            batch.append(content_line)
            if len(batch) == batch_size:
                yield batch
                batch.clear()

        yield batch


def _list_files(source_folder: str, next_token=None):
    bucket, prefix = source_folder.replace("s3://", "").split("/", 1)

    if not prefix.endswith("/"):
        prefix = f"{prefix}/"

    storage_client = _make_storage_client()

    if next_token:
        response = storage_client.list_objects_v2(ContinuationToken=next_token)
    else:
        response = storage_client.list_objects_v2(Bucket=bucket, Prefix=prefix)

    contents = (f"s3://{bucket}/{content['Key']}" for content in response['Contents'])

    for content in contents:
        yield content

    if response['IsTruncated']:
        _list_files(source_folder, response['ContinuationToken'])


def _load_json_file(file_path: str) -> Generator[Dict, None, None]:
    transport_params = _make_transport_params()

    with open(file_path, transport_params=transport_params) as source_file:
        for line in source_file.readlines():
            data = json.loads(line)
            yield data


def _make_transport_params():
    endpoint = os.environ.get("S3_ENDPOINT")

    if endpoint:
        client = _make_storage_client()
        transport_params = dict(client=client)
    else:
        transport_params = None

    return transport_params


def _make_storage_client():
    endpoint = os.environ.get("S3_ENDPOINT")
    aws_access_key_id = os.environ.get("AWS_ACCESS_KEY_ID")
    aws_secret_access_key = os.environ.get("AWS_SECRET_ACCESS_KEY")

    session = boto3.Session()
    client = session.client('s3', endpoint_url=endpoint, use_ssl=False,
                            aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
    return client