import logging

import click

from postgres_importer.loaders.json_loader import load_json
from postgres_importer.persistence.database import DatabaseClient


@click.command()
@click.option('--file-type', type=click.Choice(['JSON'], case_sensitive=False), default="JSON")
@click.option("--source", type=click.STRING, help="Source path for files to upload")
@click.option("--db-host", type=click.STRING, help="PostgresSQL host")
@click.option("--db-port", type=click.INT, default=5432, help="PostgresSQL port")
@click.option("--db-username", type=click.STRING, default="postgres", help="PostgresSQL username")
@click.option("--db-password", type=click.STRING, default="postgres", help="PostgresSQL password")
@click.option("--db-name", type=click.STRING, default="data_lake", help="PostgresSQL database")
@click.option("--db-schema", type=click.STRING, default="postgres", help="PostgresSQL schema")
@click.option("--db-table", type=click.STRING, help="PostgresSQL table")
@click.option("--batch-size", type=click.INT, default=64, help="Number of lines to accumate before insertion")
def main(file_type: str, source: str, batch_size: int, db_host: str, db_port: int, db_username: str, db_password: str,
         db_name: str, db_schema: str, db_table: str):

    if file_type == 'JSON':
        dataset = load_json(source, batch_size)
    else:
        raise Exception(f"The selected file type ({file_type}) is not supported yet")

    database_client = DatabaseClient(db_username, db_password, db_host, db_port, db_name, db_schema, db_table)

    for batch_id, batch in enumerate(dataset):
        logging.info(f"Loading batch #{batch_id}")
        database_client.insert(batch)


if __name__ == '__main__':
    main()
