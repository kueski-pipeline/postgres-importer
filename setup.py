from setuptools import setup, find_packages

setup(
    name='postgres-importer',
    version='2.0.0',
    packages=find_packages(),
    url='',
    license='',
    author='André Claudino',
    author_email='claudino@d2x.com.br',
    description='',
    install_requires=[
        "click>=8.0.3",
        "smart-open[all]>=5.2.1",
        "psycopg>=3.0.10"
    ],
    entry_points={
        'console_scripts': [
            "import-files=postgres_importer.main:main"
        ]
    }
)
