# Postgres Importer

This component is designed to import files (currently, only *JSON lines* is supported) into a postgres table.

```
Usage: import-files [OPTIONS]

Options:
  --file-type [JSON]
  --source TEXT         Source path for files to upload
  --db-host TEXT        PostgresSQL host
  --db-port INTEGER     PostgresSQL port
  --db-username TEXT    PostgresSQL username
  --db-password TEXT    PostgresSQL password
  --db-name TEXT        PostgresSQL database
  --db-schema TEXT      PostgresSQL schema
  --db-table TEXT       PostgresSQL table
  --batch-size INTEGER  Number of lines to accumate before insertion
  --help                Show this message and exit.
```