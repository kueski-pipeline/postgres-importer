DOCKER_IMAGE_NAME=andreclaudino/postgres-importer
VERSION=1.0.0
DOCKER_IMAGE_TAG=$(VERSION)
GIT_TAG=v$(VERSION)

release:
	git tag $(GIT_TAG)
	git push
	git push --tags

resources/install:
	mkdir -p resources/
	pip install -e . --upgrade --no-cache-dir
	touch resources/install

resources/uninstall:
	pip3 uninstall -y  click spandas cassandra-driver

docker/package: resources/install
	python3 setup.py bdist_wheel --dist-dir=docker/package
	rm -rf build

docker/image: docker/package
	docker build docker -f docker/Dockerfile -t $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)
	touch docker/image

docker/login:
	docker login registry.gitlab.com
	touch docker/login

docker/push: docker/image docker/login
	docker push $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)
	touch docker/push

docker/push-latest: docker/image docker/login
	docker tag $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG) $(DOCKER_IMAGE_NAME):latest
	docker push $(DOCKER_IMAGE_NAME):latest
	touch docker/push-latest

clean:
	rm -rf docker/package
	rm -rf docker/image
	rm -rf docker/push
	rm -rf docker/push-latest
	rm -rf docker/install
	rm -rf resources/install
	rm -rf resources/uninstall
	rm -rf docker/login
	rm -rf docker/resources/